# SomeTimes

> An open source time-tracking app

Check it out here : [https://sometimes.vercel.app](https://sometimes.vercel.app)

## Badges

[![Translation status](https://hosted.weblate.org/widgets/sometimes/-/sometimes-webapp/svg-badge.svg)](https://hosted.weblate.org/engage/sometimes/)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sometimes-grp_sometimes-webapp&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sometimes-grp_sometimes-webapp)
[![Netlify Status](https://img.shields.io/netlify/a4b41ef3-3b80-49c0-b3ca-db44449d2f80)](https://app.netlify.com/sites/some-times/deploys)
[![Apache License 2.0](https://img.shields.io/badge/license-Apache%20License%202.0-blue)](https://choosealicense.com/licenses/apache-2.0)

## Contributors

- [Milo Ivir](https://hosted.weblate.org/user/milotype) - Croatian and German translator
- [gallegonovato](https://hosted.weblate.org/user/gallegonovato) - Spanish translator
