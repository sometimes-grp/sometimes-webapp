import '@testing-library/jest-dom';
import { getLocaleInfos, LocaleInfos } from '../utils/requestLocale';

function getAcceptedLanguages_fr_en() {
  return ['fr-FR', 'fr', 'en-GB'];
}

test('Access correct lang', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'en',
    selectedLang: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to selected', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'base',
    selectedLang: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to selected', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'fr',
    selectedLang: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to selected', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'fr'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to lang in cookie', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'base',
    langInCookie: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: undefined,
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to lang in cookie', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'fr',
    langInCookie: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: undefined,
    langInCookie: 'en',
    locale: 'en-GB'
  });
});

test('Redirect to lang in cookie', () => {
  const localeInfos: LocaleInfos = {
    selectedLang: 'incorrect',
    langInRoute: 'base',
    langInCookie: 'en',
    locale: 'en-US'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'en',
    selectedLang: 'en',
    langInCookie: 'en',
    locale: 'en-US'
  });
});

test('Get lang from accepted langs', () => {
  const localeInfos: LocaleInfos = {};
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'fr',
    selectedLang: undefined,
    langInCookie: 'fr',
    locale: 'fr-FR'
  });
});

test('Get lang from accepted langs', () => {
  const localeInfos: LocaleInfos = {
    langInRoute: 'en'
  };
  expect(getLocaleInfos(localeInfos, getAcceptedLanguages_fr_en)).toEqual<LocaleInfos>({
    langInRoute: 'fr',
    selectedLang: undefined,
    langInCookie: 'fr',
    locale: 'fr-FR'
  });
});
