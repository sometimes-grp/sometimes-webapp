import { useCurrentDate } from '../../utils/useCurrentDate';
import clockStyles from './Clock.module.scss';

interface Props {
  className?: string;
  value: Date;
}

export default function Clock({ className, value }: Props) {
  const seconds = value.getSeconds();
  const mins = value.getMinutes();
  const hour = value.getHours();
  const minuteHandDegrees = (mins / 60) * 360 + (seconds / 60) * 6 + 90;
  const hourHandDegrees = (hour / 12) * 360 + (mins / 60) * 30 + 90;
  return (
    <div className={className + ' ' + clockStyles.clock}>
      <div className={clockStyles.clockFace}>
        <div
          className={clockStyles.hand + ' ' + clockStyles.hourHand}
          style={{ transform: `rotate(${hourHandDegrees}deg)` }}
        ></div>
        <div
          className={clockStyles.hand + ' ' + clockStyles.minuteHand}
          style={{ transform: `rotate(${minuteHandDegrees}deg)` }}
        ></div>
      </div>
    </div>
  );
}

export interface PropsRunningClock {
  className?: string;
  intervalMs?: number;
}

export function RunningClock({ className, intervalMs }: PropsRunningClock) {
  const currentDate = useCurrentDate(intervalMs);
  return <Clock className={className} value={currentDate} />;
}
