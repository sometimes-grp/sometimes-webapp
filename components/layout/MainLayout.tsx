import { faDigging } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'next-i18next';
import Head from 'next/head';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import layoutStyles from './MainLayout.module.scss';
import Menu from './menu/Menu';

interface LayoutProps {
  children?: React.ReactNode;
  title?: string;
}

export const MainLayout = ({ children, title }: LayoutProps) => {
  const currentTimer = useSelector((state: RootState) => state.timer.currentTimer);
  const hasRunningTimer = currentTimer !== undefined;
  const favicon = hasRunningTimer ? 'favicon-running' : 'favicon';
  useEffect(() => {
    if (!('setAppBadge' in navigator && 'clearAppBadge' in navigator)) return;
    if (hasRunningTimer) (navigator as any).setAppBadge();
    else (navigator as any).clearAppBadge();
  }, [hasRunningTimer]);
  const { t } = useTranslation('common');
  const appName = t('appName', 'SomeTimes');
  return (
    <>
      <Head>
        <meta name="application-name" content={appName} />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta name="apple-mobile-web-app-title" content={appName} />
        <meta name="description" content={t('appDescription', 'SomeTimes - Time tracker')} />
        <meta name="format-detection" content="telephone=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="msapplication-config" content="/browserconfig.xml" />
        <meta name="msapplication-TileColor" content="#00a300" />
        <meta name="msapplication-tap-highlight" content="no" />
        <meta name="theme-color" content="#e3ffdf" />

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />

        <link rel="icon" type="image/png" sizes="32x32" href={`/${favicon}-32x32.png`} />
        <link rel="icon" type="image/png" sizes="16x16" href={`/${favicon}-16x16.png`} />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#6fc66f" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="icon" href={`/${favicon}.ico`} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>{title ? `${title} - ${appName}` : appName}</title>
      </Head>
      <div className={layoutStyles.app}>
        <div className={layoutStyles.menu}>
          <Menu />
        </div>
        <div className={layoutStyles.rightPane}>{children}</div>
      </div>
    </>
  );
};

interface Props {
  className?: string;
  children?: React.ReactNode;
}

export const Header = ({ className, children }: Props) => (
  <div className={[layoutStyles.header, className].join(' ')}>{children}</div>
);

export const Content = ({ className, children }: Props) => (
  <div className={[layoutStyles.content, className].join(' ')}>{children}</div>
);

export const ContentUnderDevelopment = () => {
  const { t } = useTranslation();
  const underDevelopmentMessage = t('content-under-development', `Under development`);
  return (
    <Content className={layoutStyles.underDevelopment}>
      <FontAwesomeIcon className={layoutStyles.icon} icon={faDigging} />
      <span className={layoutStyles.message}>{underDevelopmentMessage}</span>
    </Content>
  );
};
