import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faClock,
  faFolderOpen,
  faGear,
  faSignInAlt,
  faTags,
  faUsers
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useMediaQuery } from 'usehooks-ts';
import { setIsMenuCollapsed } from '../../../redux/settingsSlice';
import { AppDispatch, RootState } from '../../../redux/store';
import { getDurationString, getDurationStringByHMS } from '../../../utils/duration';
import { useCurrentTime } from '../../../utils/useCurrentDate';
import { RunningClock } from '../../clock/Clock';
import menuStyles from './Menu.module.scss';
import { MenuNavLink } from './MenuItem';

function getMenuClassName(isCollapsed: boolean) {
  return isCollapsed ? `${menuStyles.menu} ${menuStyles.collapsed}` : menuStyles.menu;
}

export default function Menu() {
  const router = useRouter();
  const dispatch = useDispatch<AppDispatch>();
  const isMobile = useMediaQuery('(max-width: 767px)');
  const isMenuCollapsed = useSelector((state: RootState) =>
    isMobile ? state.settings.isMenuMobileCollapsed : state.settings.isMenuDesktopCollapsed
  );
  const setMenuCollapsed = (isCollapsed: boolean) => dispatch(setIsMenuCollapsed({ isMobile, isCollapsed }));
  const { t } = useTranslation('common');
  const currentTimer = useSelector((state: RootState) => state.timer.currentTimer);
  const hasRunningTimer = currentTimer !== undefined;
  const currentTime = useCurrentTime(hasRunningTimer ? 500 : null);
  const [duration, setDuration] = useState(getDurationStringByHMS(t, 0, 0, 0));
  if (currentTimer !== undefined) {
    // hasRunningTimer
    const durationToDisplay = getDurationString(t, currentTimer.startDate, currentTime);
    if (durationToDisplay !== duration) setDuration(durationToDisplay);
  }
  const timersMenuTitle = t('menu.timers', 'Timers');
  return (
    <div className={getMenuClassName(isMenuCollapsed)}>
      <div className={menuStyles.header}>
        <Link href="/">
          <a className={menuStyles.home} tabIndex={-1}>
            <RunningClock className={menuStyles.clock} intervalMs={15000} />
            <span className={menuStyles.appName}>{t('appName', 'SomeTimes')}</span>
          </a>
        </Link>
      </div>
      <div className={menuStyles.content}>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>{t('menu.track', 'Track')}</span>
          <MenuNavLink
            href="/"
            icon={faClock}
            title={(isActive) => (isActive || !hasRunningTimer ? timersMenuTitle : duration)}
            router={router}
          />
        </div>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>{t('menu.manage', 'Manage')}</span>
          <MenuNavLink href="/projects" icon={faFolderOpen} title={t('menu.projects', 'Projects')} router={router} />
          <MenuNavLink href="/teams" icon={faUsers} title={t('menu.teams', 'Teams')} router={router} />
          <MenuNavLink href="/tags" icon={faTags} title={t('menu.tags', 'Tags')} router={router} />
          <MenuNavLink href="/settings" icon={faGear} title={t('menu.settings', 'Settings')} router={router} />
        </div>
        <div className={menuStyles.group}>
          <span className={menuStyles.title}>{t('menu.workspace', 'Workspace')}</span>
          <MenuNavLink href="/login" icon={faSignInAlt} title={t('menu.logIn', 'Log in')} router={router} />
        </div>
      </div>
      <div className={menuStyles.footer}>
        <div
          className={menuStyles.toggleCollapse}
          tabIndex={0}
          onKeyPress={(keyboardEvent) => {
            if (keyboardEvent.key === 'Enter') setMenuCollapsed(!isMenuCollapsed);
          }}
          onClick={() => {
            setMenuCollapsed(!isMenuCollapsed);
          }}
        >
          <FontAwesomeIcon icon={isMenuCollapsed ? faAngleDoubleRight : faAngleDoubleLeft} fixedWidth />
        </div>
      </div>
    </div>
  );
}
