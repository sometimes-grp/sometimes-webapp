import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { NextRouter } from 'next/router';
import menuStyles from './Menu.module.scss';

interface Props {
  icon: IconProp;
  title: string | ((isActive: boolean) => string);
  href: string;
  router: NextRouter;
}

const getMenuItemClassName = (isActive: boolean) =>
  isActive ? `${menuStyles.link} ${menuStyles.active}` : menuStyles.link;

export const MenuNavLink = ({ icon, title, href, router }: Props) => {
  return (
    <Link href={href}>
      <a className={getMenuItemClassName(router.asPath === href)}>
        <FontAwesomeIcon icon={icon} fixedWidth />
        <span className={menuStyles.label}>{typeof title === 'string' ? title : title(router.pathname === href)}</span>
      </a>
    </Link>
  );
};
