import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'next-i18next';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TimeSlotsGroup } from '../../models/TimeSlot';
import { AppDispatch, RootState } from '../../redux/store';
import { removeTimeSlot } from '../../redux/timerSlice';
import { getDurationString, getDurationStringByMs } from '../../utils/duration';
import { useLocale } from '../../utils/useLocale';
import contentTimerStyles from './ContentTimer.module.scss';

function getDateOnly(dateTime: number) {
  const dateObj = new Date(dateTime);
  dateObj.setHours(0, 0, 0, 0);
  return dateObj.getTime();
}

export const ContentTimer = () => {
  const allTimeSlots = useSelector((state: RootState) => state.timer.timeSlots)
    .slice()
    .sort((a, b) => b?.startDate - a?.startDate);
  const dispatch = useDispatch<AppDispatch>();
  const { t } = useTranslation('common');
  const locale = useLocale();
  const fromTo = (from: number, to: number) => {
    const dateDiff = Math.round((getDateOnly(to) - getDateOnly(from)) / (1000 * 60 * 60 * 24));
    const fromToHours = t('common:format.from-to-hours', {
      defaultValue: '{{from, datetime}} - {{to, datetime}}',
      from: new Date(from),
      to: new Date(to),
      formatParams: {
        from: { timeStyle: 'short', locale },
        to: { timeStyle: 'short', locale }
      }
    });
    if (dateDiff === 0) return fromToHours;
    return (
      fromToHours +
      t('common:format.from-to-dateDiff', {
        defaultValue: ' (+{{nbDays}}d)',
        nbDays: dateDiff
      })
    );
  };
  const timeSlotsByDay = allTimeSlots.reduce((group, timeSlot) => {
    const startDate = getDateOnly(timeSlot.startDate);
    if (!group.has(startDate)) group.set(startDate, new TimeSlotsGroup());
    group.get(startDate)?.addTimer(timeSlot);
    return group;
  }, new Map<number, TimeSlotsGroup>());
  const dayFormat = new Intl.DateTimeFormat(locale.baseName, { dateStyle: 'medium' });

  return (
    <div className={contentTimerStyles.timersContent}>
      {Array.from(timeSlotsByDay, ([day, timeSlotsGroup]) => (
        <div className={contentTimerStyles.timersOfDay} key={day}>
          <span className={contentTimerStyles.headerDay}>{dayFormat.format(day)}</span>
          <span className={contentTimerStyles.headerDuration}>
            {getDurationStringByMs(t, timeSlotsGroup.totalDuration)}
          </span>
          {timeSlotsGroup.getTimers().map((timeSlot) => {
            return (
              <React.Fragment key={timeSlot.id}>
                <span className={contentTimerStyles.description}>{timeSlot.description}</span>
                <span className={contentTimerStyles.times}>
                  {fromTo(timeSlot.startDate, timeSlot.endDate as number)}
                </span>
                <span className={contentTimerStyles.duration}>
                  {getDurationString(t, timeSlot.startDate, timeSlot.endDate ?? 0)}
                </span>
                <FontAwesomeIcon
                  icon={faTrashAlt}
                  onClick={() => {
                    dispatch(removeTimeSlot(timeSlot.id));
                  }}
                />
              </React.Fragment>
            );
          })}
        </div>
      ))}
    </div>
  );
};
