module.exports = {
  createOldCatalogs: true,
  defaultNamespace: 'common',
  locales: ['base', 'de', 'en', 'es', 'fr', 'hr', 'it'],
  skipDefaultValues: (locale) => locale !== 'base',
  output: 'public/locales/$LOCALE/$NAMESPACE.json',
  input: ['{,!(__tests__|.next|.vscode|.yarn|coverage|node_modules|public|scripts)/**}/*.{js,ts,jsx,tsx}']
};
