const nextJest = require('next/jest');

const createJestConfig = nextJest({
  dir: './'
});

const customJestConfig = {
  moduleDirectories: ['node_modules', '<rootDir>/'],
  testEnvironment: 'jest-environment-jsdom',
  collectCoverageFrom: ['<rootDir>/{components,models,pages,redux,utils}/**/*.{js,ts,jsx,tsx}']
};

module.exports = createJestConfig(customJestConfig);
