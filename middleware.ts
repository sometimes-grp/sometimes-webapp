import { NextRequest, NextResponse } from 'next/server';
import { getLocaleInfos } from './utils/requestLocale';

const PUBLIC_FILE = /\.(.*)$/;
const COOKIE_SELECTED_LANG = 'selectedLang';
const COOKIE_LANG = 'lang';
const COOKIE_LOCALE = 'locale';

export function middleware(request: NextRequest) {
  if (PUBLIC_FILE.test(request.nextUrl.pathname) || request.nextUrl.pathname.includes('/api/')) return undefined;

  const currentLocaleInfos = {
    langInRoute: request.nextUrl.locale,
    selectedLang: request.cookies.get(COOKIE_SELECTED_LANG),
    langInCookie: request.cookies.get(COOKIE_LANG),
    locale: request.cookies.get(COOKIE_LOCALE)
  };

  const localeInfos = getLocaleInfos(currentLocaleInfos, () => [...getAcceptedLanguages(request)]);

  let response: NextResponse;
  if (currentLocaleInfos.langInRoute !== localeInfos.langInRoute) {
    const url = request.nextUrl.clone();
    if (localeInfos.langInRoute) url.locale = localeInfos.langInRoute;
    url.pathname =
      request.nextUrl.pathname === '/'
        ? `/${localeInfos.langInRoute}`
        : `/${localeInfos.langInRoute}${request.nextUrl.pathname}`;
    response = NextResponse.redirect(url);
  } else {
    response = NextResponse.next();
  }
  if (currentLocaleInfos.selectedLang !== localeInfos.selectedLang)
    setOrDeleteCookie(response, COOKIE_SELECTED_LANG, localeInfos.selectedLang);
  if (currentLocaleInfos.langInCookie !== localeInfos.langInCookie)
    setOrDeleteCookie(response, COOKIE_LANG, localeInfos.langInCookie);
  if (currentLocaleInfos.locale !== localeInfos.locale) setOrDeleteCookie(response, COOKIE_LOCALE, localeInfos.locale);

  return response;
}

function setOrDeleteCookie(response: NextResponse, key: string, value?: string) {
  value ? response.cookies.set(key, value, { sameSite: 'lax', path: '/' }) : response.cookies.delete(key);
}

function* getAcceptedLanguages(request: NextRequest) {
  const acceptLanguages = request.headers.get('Accept-Language')?.split(',');
  if (!acceptLanguages?.length) return;
  for (const acceptLanguage of acceptLanguages) yield acceptLanguage.split(';')[0];
}
