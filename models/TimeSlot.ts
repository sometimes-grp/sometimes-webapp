export interface TimeSlot {
  id: string;
  description: string;
  startDate: number;
  endDate?: number;
}

export class TimeSlotsGroup {
  private _timers: TimeSlot[];
  public getTimers(): readonly TimeSlot[] {
    return this._timers;
  }
  public totalDuration: number;

  public addTimer(timer: TimeSlot) {
    this._timers.push(timer);
    this.totalDuration += (timer.endDate ?? Date.now()) - timer.startDate;
  }

  constructor() {
    this._timers = [];
    this.totalDuration = 0;
  }
}
