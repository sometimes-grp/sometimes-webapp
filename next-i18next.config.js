module.exports = {
  i18n: {
    defaultLocale: 'base',
    locales: ['base', 'de', 'en', 'es', 'fr', 'hr', 'it'],
    localeDetection: false
  },
  fallbackLng: ['en', 'base'],
  returnEmptyString: false,
  reloadOnPrerender: process.env.NODE_ENV === 'development'
};
