const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
const nextConfig = {
  i18n,
  reactStrictMode: true
};

const withWorkbox = require('next-with-workbox')({
  workbox: {
    swDest: 'sw.js',
    force: true,
    maximumFileSizeToCacheInBytes: 8000000,
    exclude: [/middleware-.+-manifest\.js$/]
  },
  ...nextConfig
});
//delete withWorkbox.workbox;

module.exports = withWorkbox;
