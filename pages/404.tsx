import { GetStaticPropsContext, InferGetStaticPropsType } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Header, MainLayout } from '../components/layout';

type PageProps = InferGetStaticPropsType<typeof getStaticProps>;

const Error404Page = (_props: PageProps) => {
  const { t } = useTranslation();
  const pageTitle = t('error.page-title', 'Error {{errorCode}}', { errorCode: 404 });
  return (
    <MainLayout title={pageTitle}>
      <Header>
        <h1>{pageTitle}</h1>
      </Header>
    </MainLayout>
  );
};

export const getStaticProps = async ({ locale }: GetStaticPropsContext) => ({
  props: {
    ...(await serverSideTranslations(locale as string, ['common']))
  }
});

export default Error404Page;
