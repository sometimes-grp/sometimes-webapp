import { appWithTranslation } from 'next-i18next';
import type { AppProps } from 'next/app';
import 'normalize.css/normalize.css';
import { useEffect } from 'react';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { useTernaryDarkMode } from 'usehooks-ts';
import { Workbox } from 'workbox-window';
import { store } from '../redux/store';
import '../styles/globals.css';

const persistor = persistStore(store);

const MyApp = ({ Component, pageProps }: AppProps) => {
  useEffect(() => {
    if (!('serviceWorker' in navigator) || process.env.NODE_ENV !== 'production') {
      return;
    }
    const wb = new Workbox('/sw.js', { scope: '/' });
    wb.register().catch(function (error) {
      console.error('Workbox, registration failed with ' + error);
    });
  }, []);

  /*const { baseName: locale } = useLocale();
  useEffect(() => {
    intlPolyfill(locale);
  }, [locale]);*/

  const { isDarkMode } = useTernaryDarkMode();
  useEffect(() => {
    if (isDarkMode) document.body.classList.add('dark');
    else document.body.classList.remove('dark');
  }, [isDarkMode]);

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Component {...pageProps} />
      </PersistGate>
    </Provider>
  );
};

export default appWithTranslation(MyApp);
