import { GetStaticPropsContext } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { Content, Header, MainLayout } from '../components/layout';

const OfflinePage = () => {
  const { t } = useTranslation();
  const router = useRouter();
  return (
    <MainLayout title={t('offline.page-title', `Offline`)}>
      <Header>
        <h1>{t('offline.problem-connecting', `Problem connecting`)}</h1>
      </Header>
      <Content>
        <h2>{t('offline.check-connection-and-retry', `Check your internet connection and try again.`)}</h2>
        <button onClick={() => router.reload()}>{t('offline.refresh', `Refresh`)}</button>
      </Content>
    </MainLayout>
  );
};

export const getStaticProps = async ({ locale }: GetStaticPropsContext) => ({
  props: {
    ...(await serverSideTranslations(locale as string, ['common']))
  }
});

export default OfflinePage;
