import { GetStaticPropsContext } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { Content, Header, MainLayout } from '../components/layout';
import { ContentTimer } from '../components/timer/ContentTimer';
import { HeaderTimer } from '../components/timer/HeaderTimer';
import { RootState } from '../redux/store';
import { getDurationString, getDurationStringByHMS } from '../utils/duration';
import { useCurrentTime } from '../utils/useCurrentDate';

const HomePage = () => {
  const currentTimer = useSelector((state: RootState) => state.timer.currentTimer);
  const hasRunningTimer = currentTimer !== undefined;
  const currentTime = useCurrentTime(hasRunningTimer ? 500 : null);
  const { t } = useTranslation('common');
  const [duration, setDuration] = useState(getDurationStringByHMS(t, 0, 0, 0));
  if (currentTimer !== undefined) {
    // hasRunningTimer
    const durationToDisplay = getDurationString(t, currentTimer.startDate, currentTime);
    if (durationToDisplay !== duration) setDuration(durationToDisplay);
  }

  return (
    <MainLayout title={hasRunningTimer ? `${duration} - ${currentTimer.description}` : ''}>
      <Header>
        <HeaderTimer />
      </Header>
      <Content>
        <ContentTimer />
      </Content>
    </MainLayout>
  );
};

export const getStaticProps = async ({ locale }: GetStaticPropsContext) => ({
  props: {
    ...(await serverSideTranslations(locale as string, ['common', 'timers']))
  }
});

export default HomePage;
