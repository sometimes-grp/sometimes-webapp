import type { GetStaticProps, NextPage } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { ContentUnderDevelopment, Header, MainLayout } from '../components/layout';

const ProjectsPage: NextPage = () => {
  const { t } = useTranslation();
  const pageTitle = t('projects:page-title', 'Projects');
  return (
    <MainLayout title={pageTitle}>
      <Header>
        <h1>{pageTitle}</h1>
      </Header>
      <ContentUnderDevelopment />
    </MainLayout>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale as string, ['common', 'projects']))
  }
});

export default ProjectsPage;
