import type { GetStaticPropsContext, NextPage } from 'next';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { useTernaryDarkMode } from 'usehooks-ts';
import { Content, Header, MainLayout } from '../components/layout';
import pageStyles from '../styles/settings.module.scss';
import { useCookies } from 'react-cookie';

const SettingsPage: NextPage = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const pageTitle = t('settings:page-title', 'Settings');
  const { ternaryDarkMode, setTernaryDarkMode } = useTernaryDarkMode();
  const locales = router.locales?.filter((locale) => locale !== router.defaultLocale);
  const [cookies, setCookie, removeCookie] = useCookies(['selectedLang', 'lang']);
  return (
    <MainLayout title={pageTitle}>
      <Header>
        <h1>{pageTitle}</h1>
      </Header>
      <Content className={pageStyles['settings-page']}>
        <div className={pageStyles['settings-line']}>
          <label htmlFor="select-theme">{t('settings:theme.label', 'Theme:')}</label>
          <select
            id="select-theme"
            className={pageStyles['settings-value']}
            onChange={(ev) => setTernaryDarkMode(ev.target.value as typeof ternaryDarkMode)}
            value={ternaryDarkMode}
          >
            <option value="system">{t('settings:theme.system', 'Same as system')}</option>
            <option value="light">{t('settings:theme.light', 'Light')}</option>
            <option value="dark">{t('settings:theme.dark', 'Dark')}</option>
          </select>
        </div>
        <div className={pageStyles['settings-line']}>
          <label htmlFor="select-language">{t('settings:language.label', 'Language:')}</label>
          <select
            id="select-language"
            className={pageStyles['settings-value']}
            onChange={(ev) => {
              const selectedLanguage = ev.target.value;
              if (selectedLanguage !== 'browser') {
                setCookie('selectedLang', selectedLanguage, { sameSite: 'lax', path: '/' });
              } else {
                removeCookie('selectedLang', { sameSite: 'lax', path: '/' });
                removeCookie('lang', { sameSite: 'lax', path: '/' });
              }
              const selectedLocale = selectedLanguage !== 'browser' ? selectedLanguage : 'base';
              if (router.locale !== selectedLocale) {
                router.push({ pathname: router.pathname, query: router.query }, router.asPath, {
                  locale: selectedLocale
                });
              }
            }}
            value={cookies.selectedLang ?? 'browser'}
          >
            <option value="browser">{t('settings:language.use-browser-language', 'Use browser language')}</option>
            {locales?.map((locale) => {
              return (
                <option key={locale} value={locale}>
                  {locale}
                </option>
              );
            })}
          </select>
        </div>
      </Content>
    </MainLayout>
  );
};

export const getStaticProps = async ({ locale }: GetStaticPropsContext) => ({
  props: {
    ...(await serverSideTranslations(locale as string, ['common', 'settings']))
  }
});

export default SettingsPage;
