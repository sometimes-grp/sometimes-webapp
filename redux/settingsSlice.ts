import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type SliceState = {
  isMenuDesktopCollapsed: boolean;
  isMenuMobileCollapsed: boolean;
  language: string;
};

const settingsSlice = createSlice({
  name: 'settings',
  initialState: {
    isMenuDesktopCollapsed: false,
    isMenuMobileCollapsed: true
  } as SliceState,
  reducers: {
    setLanguage(state, action: PayloadAction<string>) {
      state.language = action.payload;
    },
    setIsMenuCollapsed(state, action: PayloadAction<{ isMobile: boolean; isCollapsed: boolean }>) {
      const isCollapsed = action.payload.isCollapsed;
      if (action.payload.isMobile) {
        state.isMenuMobileCollapsed = isCollapsed;
        if (!isCollapsed) state.isMenuDesktopCollapsed = isCollapsed;
      } else {
        state.isMenuDesktopCollapsed = isCollapsed;
        if (isCollapsed) state.isMenuMobileCollapsed = isCollapsed;
      }
    }
  }
});

export const { setIsMenuCollapsed } = settingsSlice.actions;
export default settingsSlice.reducer;
