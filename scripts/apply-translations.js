const fs = require('fs');

function getFiles(path) {
  return fs.readdirSync(path).filter((file) => fs.statSync(`${path}/${file}`).isFile());
}

const localeFiles = getFiles('lang').filter((fileName) => fileName.endsWith('.json'));
for (const localeFile of localeFiles) {
  const mergedContent = JSON.parse(fs.readFileSync(`lang/${localeFile}`, 'utf8'));
  const destFiles = Object.keys(mergedContent);
  for (const destFile of destFiles) {
    fs.writeFileSync(
      `public/locales/${localeFile.replace('.json', '')}/${destFile}.json`,
      JSON.stringify(mergedContent[destFile], null, 2) + '\n',
      'utf8'
    );
  }
}
