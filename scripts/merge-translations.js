const fs = require('fs');

function getDirectories(path) {
  return fs.readdirSync(path).filter((file) => fs.statSync(`${path}/${file}`).isDirectory());
}

function getFiles(path) {
  return fs.readdirSync(path).filter((file) => fs.statSync(`${path}/${file}`).isFile());
}

const locales = getDirectories('public/locales');
for (const locale of locales) {
  const localeFolder = `public/locales/${locale}`;
  const localeFiles = getFiles(localeFolder).filter(
    (fileName) => fileName.endsWith('.json') && !fileName.endsWith('_old.json')
  );
  const mergedLocaleFiles = {};
  for (const localeFile of localeFiles) {
    mergedLocaleFiles[localeFile.replace('.json', '')] = JSON.parse(
      fs.readFileSync(`${localeFolder}/${localeFile}`, 'utf8')
    );
  }
  fs.writeFileSync(`lang/${locale}.json`, JSON.stringify(mergedLocaleFiles, null, 2) + '\n', 'utf8');
}
