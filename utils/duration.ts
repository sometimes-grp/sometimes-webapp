import { TFunction } from 'react-i18next';

export function getDurationString(t: TFunction<'common', undefined>, start: number, end: number) {
  return getDurationStringByMs(t, end - start);
}

export function getDurationStringByMs(t: TFunction<'common', undefined>, durationMs: number) {
  if (durationMs < 0) durationMs = 0;
  const durationSec = Math.floor(durationMs / 1000);
  const seconds = durationSec % 60;
  const durationMin = Math.floor(durationSec / 60);
  const minutes = durationMin % 60;
  const hours = Math.floor(durationMin / 60);
  return getDurationStringByHMS(t, hours, minutes, seconds);
}

export function getDurationStringByHMS(
  t: TFunction<'common', undefined>,
  hours: number,
  minutes: number,
  seconds: number
) {
  const forceTwoDigits = (number: number) => (number < 10 ? '0' + number : number);
  return t('common:format.duration', {
    defaultValue: '{{h}}:{{mm}}:{{ss}}',
    h: hours,
    hh: forceTwoDigits(hours),
    m: minutes,
    mm: forceTwoDigits(minutes),
    s: seconds,
    ss: forceTwoDigits(seconds)
  });
}

export function parseSecondsFromDuration(duration: string) {
  const durationParts = duration.split(':').map((part) => parseInt(part));
  if (durationParts.length > 3 || !durationParts.every((part) => !isNaN(part))) return;
  let seconds = 0;
  let multiplier = 1;
  while (durationParts.length) {
    seconds += (durationParts.pop() ?? 0) * multiplier;
    multiplier *= 60;
  }
  return seconds;
}
