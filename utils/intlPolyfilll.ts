import { shouldPolyfill as shouldPolyfillDateTimeFormat } from '@formatjs/intl-datetimeformat/should-polyfill';
import { shouldPolyfill as shouldPolyfillgetCanonicalLocales } from '@formatjs/intl-getcanonicallocales/should-polyfill';
import { shouldPolyfill as shouldPolyfillIntlLocale } from '@formatjs/intl-locale/should-polyfill';
import { shouldPolyfill as shouldPolyfillIntlNumberFormat } from '@formatjs/intl-numberformat/should-polyfill';
import { shouldPolyfill as shouldPolyfillIntlPluralRules } from '@formatjs/intl-pluralrules/should-polyfill';

export async function intlPolyfill(locale?: string) {
  await polyfillgetCanonicalLocales();
  await polyfillIntlLocale();
  const polyfillLocale = locale ?? 'en-US';
  await polyfillIntlPluralRules(polyfillLocale);
  await polyfillIntlNumberFormat(polyfillLocale);
  await polyfillDateTimeFormat(polyfillLocale);
}

async function polyfillgetCanonicalLocales() {
  // This platform already supports Intl.getCanonicalLocales
  if (shouldPolyfillgetCanonicalLocales()) await import('@formatjs/intl-getcanonicallocales/polyfill');
}

async function polyfillIntlLocale() {
  // This platform already supports Intl.Locale
  if (shouldPolyfillIntlLocale()) await import('@formatjs/intl-locale/polyfill');
}

async function polyfillIntlPluralRules(locale: string) {
  const unsupportedLocale = shouldPolyfillIntlPluralRules(locale);
  // This locale is supported
  if (!unsupportedLocale) return;
  // Load the polyfill 1st BEFORE loading data
  await import('@formatjs/intl-pluralrules/polyfill-force');
  await import(`@formatjs/intl-pluralrules/locale-data/${unsupportedLocale}`);
}

async function polyfillIntlNumberFormat(locale: string) {
  const unsupportedLocale = shouldPolyfillIntlNumberFormat(locale);
  // This locale is supported
  if (!unsupportedLocale) return;
  // Load the polyfill 1st BEFORE loading data
  await import('@formatjs/intl-numberformat/polyfill-force');
  await import(`@formatjs/intl-numberformat/locale-data/${unsupportedLocale}`);
}

async function polyfillDateTimeFormat(locale: string) {
  const unsupportedLocale = shouldPolyfillDateTimeFormat(locale);
  // This locale is supported
  if (!unsupportedLocale) return;
  // Load the polyfill 1st BEFORE loading data
  await import('@formatjs/intl-datetimeformat/polyfill-force');
  // Parallelize CLDR data loading
  const dataPolyfills = [
    import('@formatjs/intl-datetimeformat/add-all-tz'),
    import(`@formatjs/intl-datetimeformat/locale-data/${unsupportedLocale}`)
  ];
  await Promise.all(dataPolyfills);
}
