export class LocaleInfos {
  langInRoute?: string;
  langInCookie?: string;
  selectedLang?: string;
  locale?: string;
}

const LANGS = ['de', 'en', 'es', 'fr', 'hr', 'it'];

export function getLocaleInfos(localeInfos: LocaleInfos, getAcceptedLanguages: () => string[]) {
  const nextLocaleInfos: LocaleInfos = { ...localeInfos };
  const selectedLang = getManagedLang(localeInfos.selectedLang);
  const langInCookie = getManagedLang(localeInfos.langInCookie);
  const langInRoute = getManagedLang(localeInfos.langInRoute);
  const nextLang = selectedLang ?? langInCookie;
  if (localeInfos.selectedLang && !selectedLang) nextLocaleInfos.selectedLang = nextLang;
  if (nextLang && langInCookie === nextLang && langInRoute === nextLang) return nextLocaleInfos;

  if (!nextLang) {
    const { lang, locale } = getClientAvailableLanguage(getAcceptedLanguages) ?? { lang: 'en', locale: 'en-US' };
    nextLocaleInfos.langInRoute = nextLocaleInfos.langInCookie = lang;
    nextLocaleInfos.locale = locale;
  } else if (selectedLang && (selectedLang !== langInCookie || selectedLang !== langInRoute)) {
    nextLocaleInfos.langInRoute = nextLocaleInfos.langInCookie = selectedLang;
    nextLocaleInfos.locale = getClientLocaleByLang(getAcceptedLanguages, selectedLang);
  } else {
    if (langInRoute !== nextLang) nextLocaleInfos.langInRoute = nextLang;
    if (langInCookie !== nextLang) nextLocaleInfos.selectedLang = nextLocaleInfos.langInCookie = nextLang;
    if (!nextLocaleInfos.locale || langInCookie !== nextLang)
      nextLocaleInfos.locale = getClientLocaleByLang(getAcceptedLanguages, nextLang);
  }

  return nextLocaleInfos;
}

function isManagedLang(lang: string) {
  return LANGS.includes(lang);
}

function getManagedLang(lang?: string) {
  return lang && isManagedLang(lang) ? lang : undefined;
}

function getClientAvailableLanguage(getAcceptedLanguages: () => string[]) {
  for (const clientLocale of getAcceptedLanguages()) {
    if (isManagedLang(clientLocale)) return { lang: clientLocale, locale: clientLocale };
    const localeWithoutRegion = getLocaleWithoutRegion(clientLocale);
    if (isManagedLang(localeWithoutRegion)) return { lang: localeWithoutRegion, locale: clientLocale };
  }
  return undefined;
}

function getClientLocaleByLang(getAcceptedLanguages: () => string[], lang: string) {
  for (const clientLocale of getAcceptedLanguages()) {
    if (clientLocale === lang || getLocaleWithoutRegion(clientLocale) === lang) return clientLocale;
  }
  return lang;
}

function getLocaleWithoutRegion(language: string) {
  return language.split(/[-_]/)[0];
}
