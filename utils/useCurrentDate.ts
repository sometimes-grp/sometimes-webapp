import { useState } from 'react';
import { useInterval } from 'usehooks-ts';

export function useCurrentDate(intervalMs: number | null = 1000) {
  const [currentDate, setCurrentDate] = useState(new Date());
  useInterval(() => setCurrentDate(new Date()), intervalMs);
  return currentDate;
}

export function useCurrentTime(intervalMs: number | null = 1000) {
  const [currentTime] = useUpdatableCurrentTime(intervalMs);
  return currentTime;
}

export function useUpdatableCurrentTime(intervalMs: number | null = 1000): [number, () => void] {
  const [currentDate, setCurrentDate] = useState(Date.now);
  const updateTime = () => setCurrentDate(Date.now);
  useInterval(updateTime, intervalMs);
  return [currentDate, updateTime];
}
