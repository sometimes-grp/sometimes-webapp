import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';

export function useLocale() {
  const [cookies] = useCookies(['locale', 'lang']);
  const [intlLocale, setIntlLocale] = useState<Intl.Locale>(new Intl.Locale('en'));
  useEffect(() => {
    for (const locale of [cookies.locale, cookies.lang]) {
      if (!locale) continue;
      try {
        setIntlLocale(new Intl.Locale(locale));
        return;
      } catch {}
    }
  }, [cookies.locale, cookies.lang]);
  return intlLocale;
}
